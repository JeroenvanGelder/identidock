FROM python:3.4

# Setting up the proxy
ENV HTTP_PROXY=http://proxy:8080 \
    HTTPS_PROXY=http://proxy:8080 \
    http_proxy=http://proxy:8080 \
    https_proxy=http://proxy:8080 \
    FTP_PROXY=http://proxy:8080 \
    NO_PROXY=.ccveu.local,192.168.56.1,localhost \
    no_proxy=.ccveu.local,192.168.56.1,localhost

RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi
RUN pip install Flask==0.10.1 uWSGI==2.0.8 requests==2.5.1 redis==2.10.3
WORKDIR /app
COPY app /app
COPY cmd.sh /
EXPOSE 9090 9191
USER uwsgi

CMD ["/cmd.sh"]
